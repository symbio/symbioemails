/**
 * Gulpfile
 */

/*------------------------------------*\

  Required plugins

\*------------------------------------*/
var gulp        = require('gulp');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var uncss       = require('gulp-uncss');
var sass        = require('gulp-sass');
var premailer   = require('gulp-premailer');
var del         = require('del');
var runSequence   = require('run-sequence');


/*------------------------------------*\

  Config

\*------------------------------------*/
var dest = './build';
var src = './src';
var config = {
  basePaths: {
    dest: dest,
    src: src
  },
  browserSync: {
    server: {
      // We're serving the src folder as well
      // for sass sourcemap linking
      baseDir: [dest, src]
    },
    notify: false,
    files: [
      dest + '/**',
      // Exclude Map files
      '!' + dest + '/**.map'
    ]
  },
  sass: {
    src: src + '/style/*.scss',
    dest: src + '/templates/',
    settings: {
      imagePath: '/images' // Used by the image-url helper
    }
  },
  markup: {
    src: src + '/templates/*.html',
    dest: dest
  },
  images: {
    src: src + '/images/**',
    dest: dest
  }
};

/*------------------------------------*\

  Tasks

\*------------------------------------*/

// Delete
gulp.task('clean', function(cb) { del([ './build/**' ], {
  read: false,
  dot: true,
  force: true
}, cb)});

// Browsersync
gulp.task('serve', function() {
  browserSync(config.browserSync);

  gulp.watch(config.sass.src,     ['style', 'markup', reload]);
  gulp.watch(config.sass.dest,    ['style', 'markup', reload]);
  gulp.watch(config.markup.src,   ['markup', reload]);
  gulp.watch(config.images.src,   ['images', reload]);
});

// Style
gulp.task('style', function () {
  return gulp.src(config.sass.src)
    .pipe(sass(config.sass.settings))
    .pipe(gulp.dest(config.sass.dest))
    .pipe(browserSync.reload({stream:true}));
});

// Markup
gulp.task('markup', function () {
  return gulp.src(config.markup.src)
    .pipe(premailer())
    .pipe(gulp.dest(config.markup.dest))
    .pipe(browserSync.reload({stream:true}));
});

// Images
gulp.task('images', function() {
  return gulp.src(config.images.src)
    .pipe(gulp.dest(config.images.dest));
});

gulp.task('default', ['clean'], function(cb) {
  runSequence(['serve'], 'markup', 'style', 'images', cb);
});

gulp.task('build', ['clean'], function(cb) {
  runSequence('markup', 'style', 'images', cb);
});
